import unittest

class RomanNumeralConverter(object):
	def __init__(self, roman_num):
		self.roman_num = roman_num
		self.digit_map = {"I":1, "V":5, "X":10, "L":50, "C":100}
		self.text_map = {"I":"One", "V":"Five","X":"Ten","L":"Fifty", "C":"One Hundred" }

	def convert_to_decimal(self):
		value = 0
		for char in self.roman_num:
			value += self.digit_map[char]
		return value 

	def express_in_words(self):
		
		value_text = "text"
		for char in self.roman_num:
			value_text = self.text_map[char]
		return value_text


class RomanNumeralConverterTest(unittest.TestCase):
	def test_parsing_hundred(self):
		value = RomanNumeralConverter("C")
		self.assertEqual(100, value.convert_to_decimal())

	def test_parsing_hundred(self):
		value = RomanNumeralConverter("L")
		self.assertEqual(50, value.convert_to_decimal())

	def test_parsing_one(self):
		value = RomanNumeralConverter("X")
		self.assertEqual(10, value.convert_to_decimal())
	
	def test_parsing_five(self):
		value = RomanNumeralConverter("V")
		self.assertEqual(5, value.convert_to_decimal())

	def test_parsing_one(self):
		value = RomanNumeralConverter("I")
		self.assertEqual(1, value.convert_to_decimal())

	def test_parsing_one(self):
		value = RomanNumeralConverter("I")
		self.assertEqual(1, value.convert_to_decimal())

if __name__=="__main__": 
    unittest.main()
